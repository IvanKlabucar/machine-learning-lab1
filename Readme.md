# Machine Learning

Laboratory exercise solutions for:
- Lab 1: Decision Trees
- Lab 2: SVMs
- Lab 3: Bayesian Learning and Boosting